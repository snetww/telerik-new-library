import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EncryptionUtils } from 'src/common/encryption.utils';
import { User } from 'src/database/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  providers: [UserService, EncryptionUtils],
  controllers: [UserController],
  exports: [UserService],
})
export class UserModule {}
