import { Expose } from "class-transformer";

export class UserWithReviewsDTO {
    @Expose()
    public id: string;
  
    @Expose()
    public username: string;
  
    @Expose()
    public email: string;

    @Expose()
    public numberOfReviews: number;
    
    @Expose()
    public lastLogin: Date;
  }