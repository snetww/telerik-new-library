import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserDTO } from './models/user.dto';
import { CreateUserDTO } from './models/user-create.dto';
import { EncryptionUtils } from 'src/common/encryption.utils';
import { UserLoginDTO } from './models/user-login.dto';
import { JwtPayload } from './../auth/models/jwt-payload';
import { plainToClass } from 'class-transformer';
import { User } from 'src/database/entities/user.entity';
import { UserWithReviewsDTO } from './models/user-with-reviews.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly encryptionUtils: EncryptionUtils,
  ) {}

  public async findByLoginCredentials(
    credentials: UserLoginDTO,
  ): Promise<UserDTO> {
    const user: User = await this.userRepository.findOne({
      where: { username: credentials.username },
    });

    if (!user) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }

    const areEqual = await this.encryptionUtils.comparePasswords(
      credentials.password,
      user.password,
    );

    if (!areEqual) {
      throw new HttpException('Invalid password', HttpStatus.UNAUTHORIZED);
    }
    user.lastLogin = new Date();
    this.userRepository.save(user);
    return plainToClass(UserDTO, user, {
      excludeExtraneousValues: true,
    });
  }

  public async findByPayload(payload: JwtPayload): Promise<UserDTO> {
    const user: User = await this.userRepository.findOne({
      where: { id: payload.id },
    });
    return plainToClass(UserDTO, user, {
      excludeExtraneousValues: true,
    });
  }

  public async createUser(userRegCredentials: CreateUserDTO): Promise<UserDTO> {
    const { username, password, email } = userRegCredentials;

    // Check if user already exists in the database
    const userCheck: User = await this.userRepository.findOne({
      where: [{ username: username }, { email: email }],
    });

    if (userCheck) {
      throw new HttpException(
        'User with this username or email already exists',
        HttpStatus.BAD_REQUEST,
      );
    }
    const newUser: User = this.userRepository.create({
      username,
      email,
    });

    newUser.password = await this.encryptionUtils.hashPassword(password);

    try {
      const saveUser: User = await this.userRepository.save(newUser);

      return plainToClass(UserDTO, saveUser, {
        excludeExtraneousValues: true,
      });
    } catch (err) {
      console.log(err);
    }
  }

  public async getUser(userId: string): Promise<UserWithReviewsDTO> {

    const foundUser: User = await this.userRepository.findOne({ where: { id: userId } });
    // return foundUser;
    const mappedUser: UserWithReviewsDTO = {
      id: foundUser.id,
      username: foundUser.username,
      email: foundUser.email,
      lastLogin: foundUser.lastLogin,
      numberOfReviews: (await foundUser.reviews).filter(el=> el.isDeleted == false).length
  }

  return await mappedUser


}

};
