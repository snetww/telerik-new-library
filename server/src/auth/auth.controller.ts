import { AuthService } from './auth.service';
import {
  Controller,
  Post,
  Body,
  HttpException,
  HttpStatus,
  Req,
} from '@nestjs/common';
import { UserLoginDTO } from '../user/models/user-login.dto';
import { RegistrationStatus } from './models/registration-status';
import { CreateUserDTO } from 'src/user/models/user-create.dto';
import { LoginStatus } from './models/login-status';

@Controller('')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('register')
  public async register(
    @Body() createUser: CreateUserDTO,
  ): Promise<RegistrationStatus> {
    const result: RegistrationStatus = await this.authService.register(
      createUser,
    );
    if (!result.success) {
      throw new HttpException(result.message, HttpStatus.BAD_REQUEST);
    }
    return result;
  }

  @Post('login')
  async login(
    @Body()
    userLoginCredentials: UserLoginDTO,
  ): Promise<LoginStatus> {
    return await this.authService.login(userLoginCredentials);
  }

  @Post('logout')
  async logout(@Req() req: any): Promise<any> {
    const token = req.headers.authorization.slice(7);
    return this.authService.logout(token);
  }
}
