import { JwtOptionsFactory, JwtModuleOptions } from '@nestjs/jwt';
import { JwtConstants } from '../constants/jwt-constants';

export class JwtConfigService implements JwtOptionsFactory {
  createJwtOptions(): JwtModuleOptions {
    return {
      secret: JwtConstants.secret,
      signOptions: {
        expiresIn: JwtConstants.expiry,
      },
    };
  }
}
