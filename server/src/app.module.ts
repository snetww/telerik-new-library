import { PassportModule } from '@nestjs/passport';
import { AuthModule } from './auth/auth.module';
import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule} from '@nestjs/typeorm';
import { UserModule } from './user/user.module';
import { ReviewsModule } from './reviews/reviews.module';
import { BooksModule } from './books/books.module';

@Module({
  imports: [TypeOrmModule.forRoot(),
    UserModule,
    HttpModule,
    AuthModule,
    PassportModule,
    ReviewsModule,
    BooksModule]
,
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
