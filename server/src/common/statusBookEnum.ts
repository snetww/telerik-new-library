export enum Status {
    Free = "Free",
    Unlisted = "Unlisted",
    Borrowed = "Borrowed",
}