import * as bcrypt from 'bcrypt';

export class EncryptionUtils {
  public async comparePasswords(
    userEnteredPassword: string,
    encryptedPassword: string,
  ): Promise<boolean> {
    return await bcrypt.compare(userEnteredPassword, encryptedPassword);
  }

  public async hashPassword(password: string): Promise<string> {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
  }
}
