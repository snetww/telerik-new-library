import { ShowBookDTO } from './models/show-book.dto';
import { UserDTO } from 'src/user/models/user.dto';
import { CreateBookDTO } from './models/create-book.dto';
import { Book } from './../database/entities/book.entity';
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/database/entities/user.entity';
import { plainToClass } from 'class-transformer';

@Injectable()
export class BooksService {

    public constructor(
        @InjectRepository(Book) private readonly booksRepository: Repository<Book>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>
    ) { }


    public async createBook(book: CreateBookDTO, user: UserDTO): Promise<ShowBookDTO> {

        const bookEntity: Book = this.booksRepository.create(book);
        const foundUser: User = await this.usersRepository.findOne({
            username: user.username
        });

        if (foundUser === undefined || foundUser.isDeleted) {
            throw new BadRequestException('User with such username does not exist');
        }

        bookEntity.user = Promise.resolve(foundUser);
        await this.booksRepository.save(bookEntity);

        return plainToClass(ShowBookDTO, bookEntity, {
            excludeExtraneousValues: true
        })
    }

    public async deleteBook(bookId: number): Promise<void> {

        const foundBook = await this.booksRepository.findOne({
                   where: { id: bookId },
                 });
          
                 foundBook.isDeleted = true;
                 await this.booksRepository.save(foundBook);
               
    }

    public async getAllBooks(): Promise<any> {
        const books = await this.booksRepository.find({
          where: { isDeleted: false },
        });
        return books;
      }

      public async getIndividualBook(bookId: number): Promise<any> {
        const book = await this.booksRepository.findOne({
          where: { id: bookId, isDeleted: false },
        });
        // return book;

        const mappedBook: any = {
          id: book.id,
          title: book.title,
          author: book.author,
          preview: book.preview,
          status: book.status,
          createDate: book.createDate,
          isDeleted: book.isDeleted,
          numberOfReviews: (await book.reviews).filter(el=> el.isDeleted == false).length
      }
    
      return await mappedBook
      }
    


}
