import { Status } from './../../common/statusBookEnum';
import { Expose } from 'class-transformer';

export class ShowBookDTO {

    @Expose()
    public id: number;

    @Expose()
    public title: string;

    @Expose()
    public author: string;

    @Expose()
    public preview: string;

    @Expose()
    public createDate: Date;

    @Expose()
    public status: Status;

    @Expose()
    public userId: string;

    @Expose()
    public username: string;
}
