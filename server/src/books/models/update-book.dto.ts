import { IsOptional, Length } from 'class-validator';

export class UpdateBookDTO {

    @IsOptional()
    @Length(2, 128)
    public title: string;

    @IsOptional()
    @Length(2, 32768)
    public preview: string;
    
}
