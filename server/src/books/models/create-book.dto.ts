import { Length, IsNotEmpty } from 'class-validator';

export class CreateBookDTO {

    @IsNotEmpty()
    @Length(2, 128)
    public title: string;

    @IsNotEmpty()
    @Length(2, 8000)
    public preview: string;

    @IsNotEmpty()
    @Length(2, 32768)
    public author: string;
}
