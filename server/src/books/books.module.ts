import { Book } from './../database/entities/book.entity';
import { Module } from '@nestjs/common';
import { BooksController } from './books.controller';
import { BooksService } from './books.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/database/entities/user.entity';

@Module({
  imports: [
  TypeOrmModule.forFeature([Book]),
  TypeOrmModule.forFeature([User])
],
  controllers: [BooksController],
  providers: [BooksService]
})
export class BooksModule {}
