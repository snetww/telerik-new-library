import { ShowBookDTO } from './models/show-book.dto';
import { CreateBookDTO } from './models/create-book.dto';
import { BooksService } from './books.service';
import { Controller, Post, UseGuards, HttpCode, HttpStatus, Body, Req, Param, Get } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';


@Controller('books')
export class BooksController {
    public constructor(private readonly booksService: BooksService) { }

    @Get()
     @HttpCode(HttpStatus.OK)
     public async getAllBooks(): Promise<any> {
    return await this.booksService.getAllBooks();
  }

  @Get('/:bookId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async getIndividualBook(
      @Param('bookId') bookId: number
      ): Promise<any> {
      return await this.booksService.getIndividualBook(bookId);
  }



    @Post()
    @UseGuards(AuthGuard('jwt'))
    @HttpCode(HttpStatus.CREATED)
    public async addNewBook(
        @Body() body: CreateBookDTO,
        @Req() req: any
    ): Promise<ShowBookDTO> {

        return await this.booksService.createBook(body, req.user);
    }

    @Post('/:bookId')
    @HttpCode(HttpStatus.OK)
    public async deleteBook(@Param('bookId') bookId: number): Promise<{}> {
      await this.booksService.deleteBook(bookId);

      return { msg: 'Book Deleted' };
    }

}
