import { ShowReviewDTO } from './models/show-review.dto';
import { AuthGuard } from '@nestjs/passport';
import { CreateReviewDTO } from './models/create-review.dto';
import { ReviewsService } from './reviews.service';
import { Controller, Post, HttpCode, HttpStatus, Req, Param, ParseIntPipe, Body, UseGuards, Put, Get, Delete } from '@nestjs/common';

@Controller('books')
export class ReviewsController {

    public constructor(private readonly reviewsService: ReviewsService) { }

    @Get('/:bookId/reviews')
    @HttpCode(HttpStatus.OK)
    public async getAllReviewsPerBook(
        @Param('bookId', ParseIntPipe) bookId: number): Promise<ShowReviewDTO[]> {
        return await this.reviewsService.getAllReviews(bookId);
    }

    @Get('reviews/:reviewId')
    @UseGuards(AuthGuard('jwt'))
    @HttpCode(HttpStatus.OK)
    public async getIndividualReview(
        @Param('reviewId') reviewId: number
        ): Promise<any> {
        return await this.reviewsService.getIndividualReview(reviewId);
    }

    @Get('/user/:userId/reviews')
    @UseGuards(AuthGuard('jwt'))
    @HttpCode(HttpStatus.OK)
    public async getAllUserReviews(
        @Param('userId') userId: any,
    ): Promise<ShowReviewDTO[]> {


        const userReviews: ShowReviewDTO[] = await this.reviewsService.getUserReviews(userId);

        return userReviews;
    }



    @Post('/:bookId/reviews')
    @UseGuards(AuthGuard('jwt'))
    @HttpCode(HttpStatus.CREATED)
    public async addNewReview(
        @Req() req: any,
        @Param('bookId', ParseIntPipe) bookId: number,
        @Body() body: CreateReviewDTO): Promise<{msg}> {
        await this.reviewsService.createReview(req.user, bookId, body);
        
        return { msg: 'Review Added' };
    }

    @Put('/:bookId/reviews/:reviewId')
    @UseGuards(AuthGuard('jwt'))
    @HttpCode(HttpStatus.OK)
    public async updateReview(
        @Req() req: any,
        @Param('bookId', ParseIntPipe) bookId: number,
        @Param('reviewId', ParseIntPipe) reviewId: number,
        @Body() body: CreateReviewDTO): Promise<{msg}> {
        await this.reviewsService.updateReview(req.user, bookId, reviewId, body);

        return { msg: 'Review Updated' };
    }

    @Delete('/:bookId/reviews/delete/:reviewId')
    @UseGuards(AuthGuard('jwt'))
    @HttpCode(HttpStatus.OK)
    public async deleteReview(
        @Req() req: any,
        @Param('bookId', ParseIntPipe) bookId: number,
        @Param('reviewId', ParseIntPipe) reviewId: number,
        ): Promise<{ msg }> {
        await this.reviewsService.DeleteReview(req.user, bookId, reviewId);

        return { msg: 'Review Deleted' };
    }

}
