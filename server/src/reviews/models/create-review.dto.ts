import { Length, Min, Max, IsNotEmpty } from "class-validator";
import { Expose } from "class-transformer";

export class CreateReviewDTO {
    @Length(1, 100)
    public content: string;
    
    @IsNotEmpty()
    public raiting: number;
}
