import { Expose } from "class-transformer";

export class ShowReview2DTO {
    @Expose()
    public id: number;

    @Expose()
    public content: string;

    @Expose()
    public createDate: Date;

    @Expose()
    public username: string;

    @Expose()
    public raiting: number;

    @Expose()
    public bookId: number;

    @Expose()
    public bookTitle: string;
}