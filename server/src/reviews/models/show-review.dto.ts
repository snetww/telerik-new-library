import { Book } from './../../database/entities/book.entity';
import { Expose } from "class-transformer";

export class ShowReviewDTO {
    @Expose()
    public id: number;

    @Expose()
    public content: string;

    @Expose()
    public createDate: Date;

    @Expose()
    public username: string;

    @Expose()
    public raiting: number;

    @Expose()
    public bookId: number;
}
