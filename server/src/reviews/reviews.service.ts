
import { ShowReviewDTO } from './models/show-review.dto';
import { CreateReviewDTO } from './models/create-review.dto';
import { Book } from './../database/entities/book.entity';
import { Review } from './../database/entities/review.entity';
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import { User } from 'src/database/entities/user.entity';
import { plainToClass } from 'class-transformer';
import { ShowReview2DTO } from './models/show-review2.dto';

@Injectable()
export class ReviewsService {

    constructor(
        @InjectRepository(Review) private readonly reviewRepository: Repository<Review>
    ) { }

    public async getAllReviews(bookId: number): Promise<ShowReviewDTO[]> {
        const book = await this.validateBook(bookId);

        const reviews = await this.reviewRepository.find({
              where: { book: bookId, isDeleted: false },
            });

        const mappedReviews: Promise<ShowReviewDTO>[] = reviews.map(async review => {

                const mappedReview: ShowReviewDTO = {
                    id: review.id,
                    content: review.content,
                    createDate: review.createDate,
                    raiting: review.raiting,
                    bookId: (await review.book).id,
                    username: (await review.user).username
                }
    
                return mappedReview; 
        });

        return await Promise.all(mappedReviews);
    }

    

    public async createReview(user: User, bookId: number, body: CreateReviewDTO): Promise<ShowReviewDTO> {
        const book = await this.validateBook(bookId);
        const review = this.reviewRepository.create(body);
        review.book = Promise.resolve(book);
        review.user = Promise.resolve(user);

        const createdReview = await this.reviewRepository.save(review);

        return plainToClass(ShowReviewDTO, createdReview, {
            excludeExtraneousValues: true,
        });
    }

    private async validateBook(bookId: number): Promise<Book> {
        const book = await getConnection().manager.findOne(Book, { id: bookId });

        if (!book) {
            throw new BadRequestException('Invalid input data!');
        }

        return book;
    }

    public async updateReview(
        reqUser: User,
        bookId: number,
        reviewId: number,
        body: CreateReviewDTO): Promise<{ msg: string}> {
        const { content, raiting } = body;
        const book = await this.validateBook(bookId);
        const review = await this.validateReview(reviewId);
        const userId = (await review.user).id;

        if(reqUser.id !== userId) {
            throw new BadRequestException('You are not the owner of this review!');
        }

        review.content = content;
        review.raiting = raiting;
        await this.reviewRepository.save(review);

        return {msg: `Review with id: ${review.id} has been updated!`}; 
    }

    public async DeleteReview(
        reqUser: User,
        bookId: number,
        reviewId: number,
        ): Promise<{ msg: string}> {
        const review = await this.validateReview(reviewId);
        const userId = (await review.user).id;

        if(reqUser.id !== userId) {
            throw new BadRequestException('You are not the owner of this review!');
        }

        review.isDeleted = true;
        await this.reviewRepository.save(review);

        return {msg: `Review with id: ${review.id} has been deleted!`}; 
    }



    private async validateReview(reviewId: number): Promise<Review> {
        const review = await this.reviewRepository.findOne({
            where: {
                id: reviewId,
                isDeleted: false,
            }
        });

        if (!review) {
            throw new BadRequestException('Invalid input data!');
        }

        return review;
    }

    public async getIndividualReview(reviewId: number): Promise<any> {
        const review = await this.reviewRepository.findOne({
          where: { id: reviewId, isDeleted: false },
        });
        return review;
      }

      public async getUserReviews(userId: number): Promise<ShowReview2DTO[]> {

        const foundReviews = await this.reviewRepository.find({
            where: { user: userId, isDeleted: false },
          });

        const mappedReviews: Promise<ShowReview2DTO>[] = foundReviews.map(async review => {

            const mappedReview: ShowReview2DTO = {
                id: review.id,
                content: review.content,
                createDate: review.createDate,
                raiting: review.raiting,
                bookId: (await review.book).id,
                bookTitle: (await review.book).title,
                username: (await review.user).username
            }

            return mappedReview;
        });

        return await Promise.all(mappedReviews);
    }



}
