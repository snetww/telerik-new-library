import { Review } from './review.entity';
import { Book } from './book.entity';
import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, OneToMany, ManyToMany } from "typeorm";

@Entity('users')
export class User {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column('nvarchar', { length: 12 })
    public username: string;

    @Column('nvarchar')
    public password: string;

    @Column('nvarchar', { length: 36 })
    public email: string;

    @Column({ nullable: false, default: false })
    public isDeleted: boolean;

    @UpdateDateColumn({
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP(6)',
        onUpdate: 'CURRENT_TIMESTAMP(6)',
      })
      public lastLogin: Date;
    

    @OneToMany(
        type => Book,
        book => book.user,
        { lazy: true })
    public books: Promise<Book[]>;


    @OneToMany(
        type => Review,
        reviews => reviews.user)
    public reviews: Promise<Review[]>

}
