import { Book } from './book.entity';
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, OneToOne } from 'typeorm'
import { User } from './user.entity';
import { Min, Max } from 'class-validator';

@Entity('reviews')
export class Review {
    @PrimaryGeneratedColumn('increment')
    public id: number;

    @Column('nvarchar',{ nullable: false })
    public content: string;

    @CreateDateColumn({ nullable: false })
    public createDate: Date;

    @Column({ nullable: false})
    public raiting: number;

    @UpdateDateColumn({ nullable: false })
    public updateDate: Date;

    @Column({ nullable: false, default: false })
    public isDeleted: boolean;

    @ManyToOne(
    type => Book, 
    book => book.reviews,
    {lazy: true})
    public book: Promise<Book>;

    @ManyToOne( 
    type => User,
    user => user.reviews,
    {eager: true})
    public user: Promise<User>;


}
