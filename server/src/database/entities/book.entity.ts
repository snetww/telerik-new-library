import { Review } from './review.entity';
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, OneToMany, ManyToOne } from "typeorm";
import { User } from "./user.entity";
import { Status } from "../../common/statusBookEnum"

@Entity('books')
export class Book {
    @PrimaryGeneratedColumn('increment')
    public id: number;

    @Column({ nullable: false, length: 128 })
    public title: string;

    @Column({ nullable: false, length: 128 })
    public author: string;

    @Column({ nullable: false, length: 900 })
    public preview: string;

    @Column({ nullable: false, default: Status.Free })
    public status: Status;

    @CreateDateColumn({ nullable: false })
    public createDate: Date;

    @Column({ nullable: false, default: false })
    public isDeleted: boolean;

    @OneToMany(
        type => Review,
        review => review.book,
        { lazy: true })
    public reviews: Promise<Review[]>;

    @ManyToOne(
        type => User,
        user => user.books,
        { lazy: true })
    public user: Promise<User>;
}
