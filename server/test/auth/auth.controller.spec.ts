import { AuthService } from '../../src/auth/auth.service';
import { AuthController } from '../../src/auth/auth.controller';
import { TestingModule, Test } from '@nestjs/testing';
import { PassportModule } from '@nestjs/passport';
import { CreateUserDTO } from '../../src/user/models/user-create.dto';
import { EncryptionUtils } from 'src/common/encryption.utils'

describe('AuthController', () => {
    let controller: AuthController;

    const authService = {
        login() { /* empty */ }
    };

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                PassportModule.register({ defaultStrategy: 'jwt' })
            ],
            controllers: [AuthController],
            providers: [
                {
                    provide: AuthService,
                    useValue: authService
                },
            ],
        }).compile();

        controller = module.get<AuthController>(AuthController);

        jest.clearAllMocks();
    });

    it('should be defined', () => {

        expect(controller).toBeDefined();
    });

    describe('loginUser should', () => {

        it('call authService.login() once with correct parameters', async () => {

            // Arrange
            const mockUser = new CreateUserDTO();
            
            jest.spyOn(authService, 'login');

            // Act
            await controller.login(mockUser);

            // Assert
            expect(authService.login).toHaveBeenCalledWith(mockUser);
            expect(authService.login).toHaveBeenCalledTimes(1);
        });
    });
});
