import { UserService } from '../../src/user/user.service';
import { TestingModule, Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../../src/database/entities/user.entity';
import { CreateUserDTO } from '../../src/user/models/user-create.dto';
import { EncryptionUtils } from 'src/common/encryption.utils'
describe('UsersService', () => {
    let service: UserService;

    const usersRepository = {
        find() { /* empty */ },
        findOne() { /* empty */ },
        save() { /* empty */ },
        create() {/* empty */ }
    };


    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                UserService,
                {
                    provide: getRepositoryToken(User),
                    useValue: usersRepository,
                },
            ],
        }).compile();

        service = module.get<UserService>(UserService);

        jest.clearAllMocks();
    });

    it('should be defined', () => {

        expect(service).toBeDefined();

    });


    describe('createUser should', () => {

        it('call usersRepository *findOne* method once with correct parameters', async () => {

            // Arrange
            const mockUser = new CreateUserDTO();
            mockUser.username = 'user1';
            mockUser.email = 'email';

            const mockUserEntity = new User();
            mockUserEntity.username = 'user1';


            jest.spyOn(usersRepository, 'findOne').mockImplementation(() => Promise.resolve(undefined));
            jest.spyOn(usersRepository, 'create').mockImplementation(() => mockUserEntity);

            // Act
            await service.createUser(mockUser);

            // Assert
            expect(usersRepository.findOne).toHaveBeenCalledWith({ where: { email: mockUser.email } });
            expect(usersRepository.findOne).toHaveBeenCalledWith({ where: { username: mockUser.username } });
            expect(usersRepository.findOne).toHaveBeenCalledTimes(2);
        });

        it('call usersRepository *create* method once with correct parameters', async () => {

            // Arrange
            const mockUser = new CreateUserDTO();
            mockUser.username = 'user1';
            mockUser.email = 'email';

            const mockUserEntity = new User();
            mockUserEntity.username = 'user1';

            jest.spyOn(usersRepository, 'findOne').mockImplementation(() => Promise.resolve(undefined));
            jest.spyOn(usersRepository, 'create').mockImplementation(() => mockUserEntity);

            // Act
            await service.createUser(mockUser);

            // Assert
            expect(usersRepository.create).toHaveBeenCalledWith(mockUser);
            expect(usersRepository.create).toHaveBeenCalledTimes(1);
        });

        it('call usersRepository *save* method once with correct parameters', async () => {

            // Arrange
            const mockUser = new CreateUserDTO();
            mockUser.username = 'user1';
            mockUser.email = 'email';

            const mockUserEntity = new User();
            mockUserEntity.username = 'user1';
           

            jest.spyOn(usersRepository, 'findOne').mockImplementation(() => Promise.resolve(undefined));
            jest.spyOn(usersRepository, 'create').mockImplementation(() => mockUserEntity);
            jest.spyOn(usersRepository, 'save').mockImplementation(() => Promise.resolve(mockUserEntity));

            // Act
            await service.createUser(mockUser);

            // Assert
            expect(usersRepository.save).toHaveBeenCalledWith(mockUserEntity);
            expect(usersRepository.save).toHaveBeenCalledTimes(1);
        });
    });

    });
