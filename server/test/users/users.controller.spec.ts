import { CreateUserDTO } from '../../src/user/models/user-create.dto';
import { UserDTO } from '../../src/user/models/user.dto';
import { UserService } from '../../src/user/user.service';
import { UserController } from './../../src/user/user.controller';
import { TestingModule, Test } from '@nestjs/testing';
import { PassportModule } from '@nestjs/passport';
import { EncryptionUtils } from 'src/common/encryption.utils'

describe('UsersController', () => {
    let controller: UserController;

    const usersService = {
        getUser() { /* empty */ },
        createUser() { /* empty */ },
    };

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                PassportModule.register({ defaultStrategy: 'jwt' })
            ],
            controllers: [UserController],
            providers: [
                {
                    provide: UserService,
                    useValue: usersService
                },
            ],
        }).compile();

        controller = module.get<UserController>(UserController);

        jest.clearAllMocks();
    });

    it('should be defined', () => {

        expect(controller).toBeDefined();
    });

    describe('getUser should', () => {

        it('call usersService.getUser() once with correct parameters', async () => {

            // Arrange
            const mockUser = new UserDTO();
            mockUser.id = "1";

            jest.spyOn(usersService, 'getUser');

            // Act
            await controller.getUser("1");

            // Assert
            expect(usersService.getUser).toHaveBeenCalledWith("1");
            expect(usersService.getUser).toHaveBeenCalledTimes(1);
        });

        it('return correct result', async () => {

            // Arrange
            const mockUser = new UserDTO;
            mockUser.id = "1";

            jest.spyOn(usersService, 'getUser').mockImplementation(() => Promise.resolve(mockUser));

            // Act
            const result = await controller.getUser("1");

            // Assert
            expect(result).toEqual(mockUser);
        });
    });


    describe('addUser should', () => {

        it('call usersService.createUser() once with correct parameters', async () => {

            // Arrange
            const mockUser = new CreateUserDTO();

            jest.spyOn(usersService, 'createUser');

            // Act
            await controller.addUser(mockUser);

            // Assert
            expect(usersService.createUser).toHaveBeenCalledWith(mockUser);
            expect(usersService.createUser).toHaveBeenCalledTimes(1);
        });

        it('return correct result', async () => {

            // Arrange
            const mockUser = new CreateUserDTO;
            const expectedUser = new UserDTO;

            jest.spyOn(usersService, 'createUser').mockImplementation(() => Promise.resolve(expectedUser));

            // Act
            const result = await controller.addUser(mockUser);

            // Assert
            expect(result).toEqual(expectedUser);
        });
    });

});
