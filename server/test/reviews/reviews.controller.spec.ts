import { UserDTO } from '../../src/user/models/user.dto';
import { ShowReviewDTO } from '../../src/reviews/models/show-review.dto';
import { CreateReviewDTO } from '../../src/reviews/models/create-review.dto';
import { ReviewsService } from '../../src/reviews/reviews.service';
import { TestingModule, Test } from '@nestjs/testing';
import { PassportModule } from '@nestjs/passport';
import { ReviewsController } from 'src/reviews/reviews.controller';
import { EncryptionUtils } from 'src/common/encryption.utils'

describe('ReviewsController', () => {
    let controller: ReviewsController;

    const reviewsService = {
        getAllReviews() { /* empty */ },
        createReview() { /* empty */ },
        updateReview() { /* empty */ },
        DeleteReview() { /* empty */ },
        getIndividualReview() { /* empty */ }
    };

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                PassportModule.register({ defaultStrategy: 'jwt' })
            ],
            controllers: [ReviewsController],
            providers: [
                {
                    provide: ReviewsService,
                    useValue: reviewsService
                },
            ],
        }).compile();

        controller = module.get<ReviewsController>(ReviewsController);

        jest.clearAllMocks();
    });

    it('should be defined', () => {

        expect(controller).toBeDefined();
    });

    describe('getAllReviews should', () => {

        it('call reviewsService.getAllReviews() once with correct parameters', async () => {

            // Arrange
            jest.spyOn(reviewsService, 'getAllReviews');
            const bookId = 1;

            // Act
            await controller.getAllReviewsPerBook(bookId);

            // Assert
            expect(reviewsService.getAllReviews).toHaveBeenCalledWith(bookId);
            expect(reviewsService.getAllReviews).toHaveBeenCalledTimes(1);
        });

        it('return result from reviewsService.getAllReviews()', async () => {

            // Arrange
            const date = new Date();
            const bookId = 1;
            const mockReviews = [
                { id: 1, content: 'test', raiting: 8, createDate: date },
                { id: 2, content: 'review', raiting: 10, createDate: date }
            ];
            jest.spyOn(reviewsService, 'getAllReviews').mockImplementation(() => Promise.resolve(mockReviews));

            // Act
            const result = await controller.getAllReviewsPerBook(bookId);

            // Assert
            expect(result).toEqual(mockReviews);
        });
    });

    describe('getIndividualReview should', () => {

        it('call reviewsService.getIndividualReview() once with correct parameters', async () => {


            // Arrange
            jest.spyOn(reviewsService, 'getIndividualReview');

            // Act
            await controller.getIndividualReview(1);

            // Assert
            expect(reviewsService.getIndividualReview).toHaveBeenCalledWith(1);
            expect(reviewsService.getIndividualReview).toHaveBeenCalledTimes(1);
        });

        it('return individual review by id', async () => {

            // Arrange
            const date = new Date();

            const expectedResult = { id: 1, content: 'test', raiting: 10, createDate: date };

            jest.spyOn(reviewsService, 'getIndividualReview').mockImplementation(() => Promise.resolve(expectedResult));

            // Act
            const result = await controller.getIndividualReview(1);

            // Assert
            expect(result).toBe(expectedResult);
        });
    });

    describe('addNewReview should', () => {

        it('call reviewsService.createReview() once with correct parameters', async () => {

            // Arrange
            const mockReview = new CreateReviewDTO();
            const mockUser = new UserDTO();
            const bookId = 1;

            jest.spyOn(reviewsService, 'createReview');

            // Act
            await controller.addNewReview( mockUser,bookId, mockReview);

            // Assert
            expect(reviewsService.createReview).toHaveBeenCalledWith( mockUser,bookId, mockReview);
            expect(reviewsService.createReview).toHaveBeenCalledTimes(1);
        });

        it('return correct result', async () => {

            // Arrange
            const date = new Date();
            const bookId = 1;

            const mockReview: CreateReviewDTO = {
                content: 'test',
                raiting: 10
            }

            const expectedReview: ShowReviewDTO = {
                id: 1,
                content: 'test',
                raiting: 10,
                createDate: date,
                username: 'snetww',
                bookId: 1

            }
            const mockUser = new UserDTO();

            jest.spyOn(reviewsService, 'createReview').mockImplementation(() => Promise.resolve(expectedReview));

            // Act
            const result = await controller.addNewReview(mockUser, bookId, mockReview);

            // Assert
            expect(result).toEqual(expectedReview);
        });
    });

    describe('updateReview should', () => {

        it('call reviewsService.updateReview() once with correct parameters', async () => {

            // Arrange
            const mockReview = new CreateReviewDTO();
            const mockUser = new UserDTO();
            const bookId = 1;
            const reviewId = 1;
            const body = {
                content: "dada",
                raiting: 10
            }

            jest.spyOn(reviewsService, 'updateReview');

            // Act
            await controller.updateReview(mockUser, bookId, reviewId, body);

            // Assert
            expect(reviewsService.updateReview).toHaveBeenCalledWith(mockUser, bookId, reviewId, body);
            expect(reviewsService.updateReview).toHaveBeenCalledTimes(1);
        });

        it('return correct message', async () => {

            // Arrange
            const mockReview = new CreateReviewDTO();
            const expectedResult = { msg: 'Review Updated' };
            const mockUser = new UserDTO();
            const bookId = 1;
            const reviewId = 1;
            const body = {
                content: "dada",
                raiting: 10
            }

            jest.spyOn(reviewsService, 'updateReview');

            // Act
            const result = await controller.updateReview(mockUser, bookId, reviewId, body);

            // Assert
            expect(result).toEqual(expectedResult);
        });
    });

    describe('deleteReview should', () => {

        it('call reviewsService.deleteReview() once with correct parameters', async () => {

            // Arrange
            const mockUser = new UserDTO();
            const bookId = 1;
            const reviewId = 1;
            jest.spyOn(reviewsService, 'DeleteReview');

            // Act
            await controller.deleteReview(mockUser, bookId, reviewId);

            // Assert
            expect(reviewsService.DeleteReview).toHaveBeenCalledWith(mockUser, bookId, reviewId);
            expect(reviewsService.DeleteReview).toHaveBeenCalledTimes(1);
        });

        it('return correct message', async () => {

            // Arrange
            const expectedResult = { msg: 'Review Deleted' };
            const mockUser = new UserDTO();
            const bookId = 1;
            const reviewId = 1;
            jest.spyOn(reviewsService, 'DeleteReview');

            // Act
            const result = await controller.deleteReview(mockUser, bookId, reviewId);

            // Assert
            expect(result).toEqual(expectedResult);
        });
    });
});
