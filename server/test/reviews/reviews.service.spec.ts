import { UserDTO } from '../../src/user/models/user.dto';
import { CreateReviewDTO } from '../../src/reviews/models/create-review.dto';
import { ShowReviewDTO } from '../../src/reviews/models/show-review.dto';
import { Book } from '../../src/database/entities/book.entity';
import { Review } from '../../src/database/entities/review.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { TestingModule, Test } from '@nestjs/testing';
import { User } from '../../src/database/entities/user.entity';
import { ReviewsService } from 'src/reviews/reviews.service';
import { EncryptionUtils } from 'src/common/encryption.utils'

describe('ReviewsService', () => {
    let service: ReviewsService;

    const reviewsRepository = {
        find() { /* empty */ },
        findOne() { /* empty */ },
        save() { /* empty */ },
        create() {/* empty */ }
    };
    const usersRepository = {
        findOne() { /* empty */ }
    };


    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                ReviewsService,
                {
                    provide: getRepositoryToken(Review),
                    useValue: reviewsRepository,
                },
                {
                    provide: getRepositoryToken(User),
                    useValue: usersRepository,
                },
            ],
        }).compile();

        service = module.get<ReviewsService>(ReviewsService);

        jest.clearAllMocks();
    });

    it('should be defined', () => {

        expect(service).toBeDefined();

    });

    describe('getAllReviews should', () => {

        it('call postsRepository *find* method once with correct parameters', async () => {

            // Arrange

            const mockUser = new User();
            const mockBook = new Book()
            mockBook.id = 1;
            const mockDate = new Date()
            const mockReviewEntity: Review = {
                id: 1,
                content: 'test',
                raiting: 10,
                createDate: new Date(),
                updateDate: mockDate,
                isDeleted: false,
                user: Promise.resolve(mockUser),
                book: Promise.resolve(mockBook)
            };

            const bookId = 1;

            jest.spyOn(reviewsRepository, 'find').mockImplementation(() => Promise.resolve([mockReviewEntity]));

            // Act

            await service.getAllReviews(bookId);


            // Assert
            expect(reviewsRepository.find).toHaveBeenCalledWith({ where: { isDeleted: false } });
            expect(reviewsRepository.find).toHaveBeenCalledTimes(1);
        });

        it('return transformed array of ShowReviewDTO objects', async () => {
            // Arrange
            const date = new Date();

            const mockUser = new User();
            mockUser.id = "1"
            mockUser.username = 'user1'
            const mockBook = new Book()
            mockBook.id = 1;
            const bookId = mockBook.id
            const mockDate = new Date()

            const mockReviewEntity: Review = {
                id: 1,
                content: 'test',
                raiting: 10,
                createDate: new Date(),
                updateDate: mockDate,
                isDeleted: false,
                user: Promise.resolve(mockUser),
                book: Promise.resolve(mockBook)
            };

            const mockReview: ShowReviewDTO = {
                id: 1,
                content: 'test',
                raiting: 10,
                createDate: date,
                bookId: 1,
                username: 'user1',
            };

            jest.spyOn(reviewsRepository, 'find').mockImplementation(() => Promise.resolve([mockReviewEntity]));

            // Act
            const result = await service.getAllReviews(bookId);

            // Assert
            expect(result.length).toEqual(1);
            expect(result[0]).toEqual(mockReview);
        });
    });

    describe('createReview should', () => {

        it('call postsRepository *create* method once with correct parameters', async () => {

            // Arrange
            const mockReview = new CreateReviewDTO();
            const mockUser2 = {
                id: "1",
                username: 'snetww',
                email: "neshto@abv.bg"
            };
            const mockReviewEntity = new Review();
            const mockUser = new User();
            const bookId = 1;
            const body = {
                content: "dada",
                raiting: 10
            }

            jest.spyOn(reviewsRepository, 'create').mockImplementation(() => mockReviewEntity);
            jest.spyOn(reviewsRepository, 'save');

            // Act
            await service.createReview(mockUser, bookId, body);

            // Assert
            expect(reviewsRepository.create).toHaveBeenCalledWith(mockReview);
            expect(reviewsRepository.create).toHaveBeenCalledTimes(1);
        });
        it('call reviewsRepository *save* method once with correct parameters', async () => {

            // Arrange
            const mockReview = new CreateReviewDTO();
            const mockUserEntity = new User();
            const mockUser = new User;
            const body = {
                content: "dada",
                raiting: 10
            }
            const bookId = 1;

            jest.spyOn(usersRepository, 'findOne').mockImplementation(() => Promise.resolve(mockUserEntity));
            jest.spyOn(reviewsRepository, 'create').mockImplementation(() => mockReview)
            jest.spyOn(reviewsRepository, 'save');

            // Act
            await service.createReview(mockUser, bookId, body);


            // Assert
            expect(reviewsRepository.save).toHaveBeenCalledWith(mockReview);
            expect(reviewsRepository.save).toHaveBeenCalledTimes(1);
        });

        it('return the transformed review created by the repository', async () => {

            // Arrange
            const date = new Date();

            const mockReview: CreateReviewDTO = {
            
                content: 'testing',
                raiting: 10
            };

            const expectedReview: ShowReviewDTO = new ShowReviewDTO();
            expectedReview.id = 1;
            expectedReview.content = 'test';
            expectedReview.raiting = 10;
            expectedReview.createDate = date;

            const body = {
                content: "test",
                raiting: 10
            }
            const bookId = 1;

            const mockReviewEntity = new Review();
            expectedReview.id = 1;
            expectedReview.content = 'test';
            expectedReview.raiting = 10;
            expectedReview.createDate = date;

            const mockUserEntity = new User();

            const mockUser = new User();

            jest.spyOn(reviewsRepository, 'create').mockImplementation(() => mockReviewEntity);
            jest.spyOn(usersRepository, 'findOne').mockImplementation(() => Promise.resolve(mockUserEntity));
            jest.spyOn(reviewsRepository, 'save');

            // Act
            const result = await service.createReview(mockUser, bookId, body);

            // Assert
            expect(result).toBeInstanceOf(ShowReviewDTO);
            expect(result).toEqual(expectedReview);
        });
    });

    describe('updateReview should', () => {

        it('call reviewsRepository *save* method once with correct parameters', async () => {

            // Arrange
            const mockReview = new CreateReviewDTO();

            const mockUserEntity = new User();
            mockUserEntity.username = 'user1';

            const mockReviewEntity = new Review();
            mockReviewEntity.user = Promise.resolve(mockUserEntity);
            const body = {
                content: "test",
                raiting: 10
            }
            const bookId = 1;
            const reviewId = 1;
            
            jest.spyOn(reviewsRepository, 'save').mockImplementation(() => Promise.resolve(mockReviewEntity));;

            // Act
            await service.updateReview(mockUserEntity , bookId, reviewId, body);

            // Assert
            expect(reviewsRepository.save).toHaveBeenCalledWith(mockReviewEntity);
            expect(reviewsRepository.save).toHaveBeenCalledTimes(1);
        });
    });
   
});
