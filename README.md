# Telerik Academy New Library

<!-- ![alt text]() -->

Used Technologies: TypeScript, Node.js, NestJS, TypeORM, MySQL, Angular, Angular Material UI, UiKit


# Public part - functionalities
* Log in
* Register
* View some Public Books & Top 10 Writers of all time!

# Private part - functionalities
<!-- ![alt text]() -->
* Have to authenticate yourself(login)!
* View all the books that are in the library.
* View the Status of the books: Free, Borrowed. Unlisted.
* View Detail page of every book, where you can find information about the book.
* View and read all the reviews that other users left.
* Make your own review - Also you can edit and delete it!
* The review contains - review's content and raiting for the book 0/5 stars!
* You can view the Library Staff(In Staff Page).
* You can view top 3 Weiters of all time(In Top 3 Writers Page).
* You can see some personal/user information on your user-panel.
* You can see if you are banned or not in you user-panel!
* You can logout!


# Installation
1. Clone the repo
2. npm install in both front-end and back-end
3. Run mysql database and make schema librarydb
4. In back-end run npm run start:dev to create entities
5. Enter your database options in ormconfig.json file in root folder
 

 example


    {
        "type": "mysql",
        "host": "localhost",
        "port": 3306,
        "username": "root",
        "password": "root",
        "database": "librarydb",
        "entities": ["dist/**/*.entity{.ts,.js}"],
        "synchronize": true
}

6. In front-end run ng serve
7. Open in browser http://localhost:4200
8. npm run test - for the tests
9. Enjoy!
