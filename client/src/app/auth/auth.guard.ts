import { NotificationService } from './../common/services/notification.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(
        private readonly authService: AuthService,
        private readonly router: Router,
        private readonly notificationService: NotificationService
    ) { }

    public canActivate(): boolean {
        if (!this.authService.getUserDataIfAuthenticated()) {
            this.notificationService.error(
                `You must be logged to access this page!`
            );
            this.router.navigate(['auth/login']);

            return false;
        }

        return true;
    }
}
