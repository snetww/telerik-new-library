import { LoginUserDTO } from '../../users/models/login-user.dto';
import { Router } from '@angular/router';
import { NotificationService } from './../../common/services/notification.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly formBuilder: FormBuilder,
    private readonly notification: NotificationService,
    private readonly router: Router
  ) { }

  public ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: [
        '',
        [Validators.required, Validators.minLength(2), Validators.maxLength(12)]
      ],
      password: [
        '',
        [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/)]
      ]
    });
  }

  public loginUser(user: LoginUserDTO): void {
    this.authService.login(user)
      .subscribe(() => {
        this.notification.success('Login successful');
        this.router.navigate(['/books']);
      },
        () => this.notification.error('Invalid username or password')
      );
  }
}
