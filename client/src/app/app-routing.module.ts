import { StaffPageComponent } from './components/staff-page/staff-page.component';
import { WritersPageComponent } from './components/writers-page/writers-page.component';
import { BookDetailpageComponent } from './components/book-detailpage/book-detailpage.component';
import { AllBooksComponent } from './components/books/all-books/all-books.component';
import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';
import { ServerErrorComponent } from './components/server-error/server-error.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AuthGuard } from './auth/auth.guard';
import { SearchPageComponent } from './components/search-page/search-page.component';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'auth/login', component: LoginComponent },
    { path: 'auth/register', component: RegisterComponent },
    { path: 'books', component: AllBooksComponent, canActivate: [AuthGuard]},
    { path: 'writers', component: WritersPageComponent, canActivate: [AuthGuard]},
    { path: 'staff', component: StaffPageComponent, canActivate: [AuthGuard]},
    { path: 'item/:productId', component: BookDetailpageComponent, canActivate: [AuthGuard] },
    { path: 'users', loadChildren: () => import('./users/users.module').then(m => m.UsersModule) },
    { path: 'search', component: SearchPageComponent, canActivate: [AuthGuard] },
    { path: 'not-found', component: NotFoundComponent },
    { path: 'server-error', component: ServerErrorComponent },
    { path: '**', redirectTo: '/not-found' }

];

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
