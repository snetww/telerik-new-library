import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ShowUserDTO } from './models/show-user.dto';
import { CONFIG } from '../common/config';
import { ShowReviewDetailsDTO } from '../components/reviews/models/show-review-details.dto';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private readonly httpClient: HttpClient) { }

  public getUser(userId: number): Observable<ShowUserDTO> {

    return this.httpClient.get<ShowUserDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}`);
  }
  
  public getAllUserReviews(userId: string): Observable<ShowReviewDetailsDTO[]> {

    return this.httpClient.get<ShowReviewDetailsDTO[]>(`${CONFIG.DOMAIN_NAME}/books/user/${userId}/reviews`);
  }

}
