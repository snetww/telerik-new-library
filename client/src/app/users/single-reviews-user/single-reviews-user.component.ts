import { NotificationService } from './../../common/services/notification.service';
import { ShowReviewDetailsDTO } from 'src/app/components/reviews/models/show-review-details.dto';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReviewsService } from 'src/app/components/reviews/reviews.service';
import { AuthService } from 'src/app/auth/auth.service';
import { LoggedInUserDTO } from '../models/logged-in-user.dto';

@Component({
  selector: 'app-single-reviews-user',
  templateUrl: './single-reviews-user.component.html',
  styleUrls: ['./single-reviews-user.component.css']
})
export class SingleReviewsUserComponent implements OnInit {
  @Input()
  public userReview: ShowReviewDetailsDTO;
  @Output() 
  public deleteEvent: EventEmitter<number> = new EventEmitter();
  
  public hidden = false;
  public isRightPerson: boolean;
  // public personReview = review.username;
  public loggedUserData: string;
  public loggedUserSubscription: Subscription;
  panelOpenState = false;
  public updateReviewForm: FormGroup;

  constructor(private readonly reviewsService: ReviewsService,
    private readonly notification: NotificationService,
    public readonly authService: AuthService,
    private readonly formBuilder: FormBuilder) { }

  ngOnInit(): void {

    this.loggedUserSubscription = this.authService.loggedUser$.subscribe(
      (data: LoggedInUserDTO) => {
        (this.loggedUserData = data.username)
      });


      const checkIfRightPerson = () => {
        if(this.userReview.username != this.loggedUserData) {
          return this.isRightPerson = false;
        } else {
          this.isRightPerson = true;
        }
      }
      checkIfRightPerson()

      this.updateReviewForm = this.formBuilder.group({
        textOfReview: [
          '',
          [Validators.required, Validators.minLength(2), Validators.maxLength(100)]
        ],
        raitingOfReview: [
          '',
          [Validators.required, Validators.min(0), Validators.max(5)]
        ],
      });
  }
  public deleteReview(): void {
    this.reviewsService.deleteReview(this.userReview.bookId, this.userReview.id).subscribe(() => {
     this.notification.success('Review Deleted!');
     this.deleteEvent.emit(this.userReview.id)
     
   },
     () => this.notification.error('Invalid type of data!')
   );
 };

 public updateReview(bookId: number, reviewId: number, contentX: string, raitingX: number): void {
   this.reviewsService.updateReview(bookId, reviewId, contentX, raitingX)
     .subscribe(() => {
       this.notification.success('Review updated!');
       this.deleteEvent.emit(this.userReview.id)
       
     },
       () => this.notification.error('You have no permissions!')
     );
   }
}
