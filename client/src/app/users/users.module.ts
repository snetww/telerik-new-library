import { UsersService } from './users.service';
import { NgModule } from '@angular/core';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UsersRoutingModule } from './users-routing.module';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { SingleReviewsUserComponent } from './single-reviews-user/single-reviews-user.component';
import { AllReviewsUserComponent } from './all-reviews-user/all-reviews-user.component';



@NgModule({
  declarations: [
    UserProfileComponent,
    SingleReviewsUserComponent,
    AllReviewsUserComponent,
  ],
  imports: [
    UsersRoutingModule,
    FormsModule,
    MaterialModule,
    InfiniteScrollModule
  ],
  providers: [UsersService],
})
export class UsersModule { }
