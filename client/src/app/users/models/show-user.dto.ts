export class ShowUserDTO {

    public id: number;

    public username: string;

    public email: string;

    public lastLogin: Date;

    public numberOfReviews: Date;
}
