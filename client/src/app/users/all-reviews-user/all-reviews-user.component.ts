import { ShowReviewDetailsDTO } from 'src/app/components/reviews/models/show-review-details.dto';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-all-reviews-user',
  templateUrl: './all-reviews-user.component.html',
  styleUrls: ['./all-reviews-user.component.css']
})
export class AllReviewsUserComponent implements OnInit {

  public userReviews: ShowReviewDetailsDTO[] = [];

  private idOfUser: string;

  public constructor(
    private readonly route: ActivatedRoute,
    private readonly usersService: UsersService
  ) { }

  public ngOnInit(): void {

    const urlAddres = location.href;
    const id = urlAddres.split('/').slice(-1).join('');
    this.idOfUser = id;
    this.usersService.getAllUserReviews(id).subscribe((foundReviews) => {
      this.userReviews = foundReviews;
    });
  }
  public refresh(reviewId: number): void {
    this.usersService.getAllUserReviews(this.idOfUser).subscribe((foundReviews) => {
      this.userReviews = foundReviews;
    });
  }

}
