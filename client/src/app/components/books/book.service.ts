import { BookShowDTO } from './models/book-show.dto';
import { BookAddDTO } from './models/book-add.dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { CONFIG } from 'src/app/common/config';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  public readonly showBookSubject$ = new Subject<BookShowDTO[]>();
  constructor(
    private readonly httpClient: HttpClient
    ) { }

  public getIndividualBook(id: number): Observable<any> {
    return this.httpClient.get(`${CONFIG.DOMAIN_NAME}/books/${id}`);
  }


  public getAllBooks(): Observable<any> {
    return this.httpClient.get(`${CONFIG.DOMAIN_NAME}/books`);
  }

  public getAllBooks2(): void {
    this.httpClient
      .get(`${CONFIG.DOMAIN_NAME}/books`)
      .subscribe((data) => this.showBookSubject$.next(data as any));
  }

  
  public addBook(book: BookAddDTO): Observable<any> {
    return this.httpClient.post(`${CONFIG.DOMAIN_NAME}/books`, book);
  }

}
