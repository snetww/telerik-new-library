export class BookShowDTO {

    public id: number;

    public author: string;

    public title: string;

    public preview: string;

    public status: string;

    public createDate: Date;

    public numberOfReviews: number;
}

