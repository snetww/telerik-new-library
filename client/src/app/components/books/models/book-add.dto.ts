export class BookAddDTO {

    public title: string;

    public preview: string;

    public author: string;

}
