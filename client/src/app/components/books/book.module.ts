import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllBooksComponent } from './all-books/all-books.component';
import { SingleBooksComponent } from './single-books/single-books.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { ReviewsModule } from '../reviews/reviews.module';



@NgModule({
  declarations: [AllBooksComponent, SingleBooksComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    ReviewsModule
  ],
  exports: [AllBooksComponent, SingleBooksComponent]
})
export class BookModule { }
