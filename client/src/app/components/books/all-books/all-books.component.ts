import { BookService } from './../book.service';
import { BookShowDTO } from './../models/book-show.dto';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-all-books',
  templateUrl: './all-books.component.html',
  styleUrls: ['./all-books.component.css']
})
export class AllBooksComponent implements OnInit {
  public books: BookShowDTO[] = [];
  constructor(private readonly bookService: BookService) { }

  ngOnInit(): void {
    // this.bookService.getAllBooks().subscribe(data => this.books = data);
    this.bookService.showBookSubject$.subscribe(data => this.books = data);
    this.bookService.getAllBooks2();

  }

}
