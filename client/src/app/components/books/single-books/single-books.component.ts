import { BookShowDTO } from './../models/book-show.dto';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-single-books',
  templateUrl: './single-books.component.html',
  styleUrls: ['./single-books.component.css']
})
export class SingleBooksComponent implements OnInit {

  @Input()
  public book: BookShowDTO;
  
  constructor() { }

  ngOnInit(): void {
  }

}
