import { AddReviewDTO } from './../reviews/models/add-review.dto';
import { ReviewsService } from './../reviews/reviews.service';
import { Router } from '@angular/router';
import { NotificationService } from './../../common/services/notification.service';
import { BookService } from './../books/book.service';
import { BookShowDTO } from './../books/models/book-show.dto';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-book-detailpage',
  templateUrl: './book-detailpage.component.html',
  styleUrls: ['./book-detailpage.component.css']
})
export class BookDetailpageComponent implements OnInit {
  public book: BookShowDTO;
  panelOpenState = false;
  public pageBookId;
  // public textOfReview: string;
  // public raitingOfReview: string;
  public addReviewForm: FormGroup;

  constructor(
    private readonly bookService: BookService,
    private readonly formBuilder: FormBuilder,
    private readonly notification: NotificationService,
    private readonly reviewService: ReviewsService

  ) { }

  ngOnInit(): void {
    const urlAddres = location.href;
    const id = urlAddres.split('/').slice(-1).join('');
    this.pageBookId =+id
    this.bookService.getIndividualBook(+id).subscribe((foundBook) => {
      this.book = foundBook;
    });

    this.addReviewForm = this.formBuilder.group({
      textOfReview: [
        '',
        [Validators.required, Validators.minLength(2), Validators.maxLength(100)]
      ],
      raitingOfReview: [
        '',
        [Validators.required, Validators.min(0), Validators.max(5)]
      ],
    });
    
  }
  

  public addReview(pageBookId: number, contentX: string, raitingX: number): void {
    this.reviewService.addReview(pageBookId, contentX, raitingX)
      .subscribe(() => {
        this.notification.success('Review added!');
        
      },
        () => this.notification.error('You have no permissions!')
      );
    }
    
  }

