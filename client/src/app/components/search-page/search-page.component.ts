import { BookService } from './../books/book.service';
import { BookShowDTO } from './../books/models/book-show.dto';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent implements OnInit {
  public keyword = '';
  public value = '';
  public filterValue = '1';
  public books: BookShowDTO[];
  public allBooks: BookShowDTO[];

  constructor(
    private readonly bookService: BookService
  ) { }

  ngOnInit(): void {
    this.bookService.showBookSubject$.subscribe(data => {
      this.books = data;
      this.allBooks = data;
});
    this.bookService.getAllBooks2();

  }

  public triggerSearch() {
    this.books = this.allBooks;
    this.books = this.allBooks.filter(el => el.title?.toLowerCase().includes(this.keyword?.toLowerCase()) ||
    el.author?.toLowerCase().includes(this.keyword?.toLowerCase()));
    }
  

}
