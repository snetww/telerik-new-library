import { AuthService } from '../../auth/auth.service';
import { LoggedInUserDTO } from '../../users/models/logged-in-user.dto';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-navigation',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public keyword = '';

  public loggedUserData: LoggedInUserDTO;
  private loggedUserSubscription: Subscription;

  constructor(
    private readonly router: Router,
    public readonly authService: AuthService
  ) { }

  public ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUser$.subscribe(
      (data: LoggedInUserDTO) => (this.loggedUserData = data)
    );
  }

  public searchPost(): void {
    this.router.navigate(['/posts/search', this.keyword]);
  }

  public searchPostWithEnter(event): void {
    if (event.which === 13) {
      this.searchPost();
    }
  }

  public logout(): void  {
  this.authService.logOutUser();

  this.loggedUserData = undefined;
}
}
