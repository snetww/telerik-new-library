import { AppService } from './../../app.service';
import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  name: string;
  position: number;
  books: string;
  year: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'William Shakespeare', books: "King Lear, Romeo & Juliet, Hamlet", year: '1585–1613'},
  {position: 2, name: 'Fyodor Dostoevsky', books: "Crime and Punishment, Demons, The Idiot", year: '1846–1880'},
  {position: 3, name: 'Leo Tolstoy', books: "War and Peace, Anna Karenina, A Confession", year: '1847–1910'},
  {position: 4, name: 'Homer', books: 'Iliad, Odyssey', year: '-'},
  {position: 5, name: 'Charles Dickens', books: 'Great Expectations, A Christmas Carol, Bleak House', year: '1812-1870'},
  {position: 6, name: 'Miguel de Cervantes', books: 'Don Quixote, Entremeses', year: '1547-1616'},
  {position: 7, name: 'Ernest Hemingway', books: 'The Old Man and the Sea, Farewell to Arms, The Sun Also Rises', year: '1899-1961'},
  {position: 8, name: 'George Orwell', books: '1984, Animal Farm', year: '1903-1950'},
  {position: 9, name: 'Victor Hugo', books: 'Les Misérables, The Hunchback of Notre-Dame, Odes et Ballades', year: '1829–1883'},
  {position: 10, name: 'J. R. R. Tolkien', books: 'The Lord of the Rings, The Hobbit, The Silmarillion', year: '1892-1973'},
];

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'books', 'year'];
  dataSource = ELEMENT_DATA;

  public constructor(
    private readonly appService: AppService
  ) { }

  public ngOnInit(): void {


  }
}
