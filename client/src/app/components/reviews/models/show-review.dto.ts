export class ShowReviewDTO {

    public id: number;

    public content: string;

    public createDate: Date;

    public username: string;

    public raiting: number;

    public bookId: number;

}
