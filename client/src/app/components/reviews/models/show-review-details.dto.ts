
export class ShowReviewDetailsDTO {

    public id: number;

    public content: string;

    public raiting: number;

    public createDate: Date;

    public username: string;

    public bookId: number;

    public bookTitle: string;

}
