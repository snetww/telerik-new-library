import { Subscription } from 'rxjs';
import { ReviewsService } from './../reviews.service';
import { ShowReviewDTO } from './../models/show-review.dto';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NotificationService } from 'src/app/common/services/notification.service';
import { LoggedInUserDTO } from 'src/app/users/models/logged-in-user.dto';
import { AuthService } from 'src/app/auth/auth.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-single-review',
  templateUrl: './single-review.component.html',
  styleUrls: ['./single-review.component.css']
})
export class SingleReviewComponent implements OnInit {
  
  @Input()
  public review: ShowReviewDTO;

  @Output() 
  public deleteEvent: EventEmitter<number> = new EventEmitter();
  
  public hidden = false;
  public isRightPerson: boolean;
  // public personReview = review.username;
  public loggedUserData: string;
  public loggedUserSubscription: Subscription;
  panelOpenState = false;
  public updateReviewForm: FormGroup;

  constructor(
    private readonly reviewsService: ReviewsService,
    private readonly notification: NotificationService,
    public readonly authService: AuthService,
    private readonly formBuilder: FormBuilder
    
    ) { }

  ngOnInit(): void {

    this.loggedUserSubscription = this.authService.loggedUser$.subscribe(
      (data: LoggedInUserDTO) => {
        (this.loggedUserData = data.username)
      });


      const checkIfRightPerson = () => {
        if(this.review.username != this.loggedUserData) {
          return this.isRightPerson = false;
        } else {
          this.isRightPerson = true;
        }
      }
      checkIfRightPerson()

      this.updateReviewForm = this.formBuilder.group({
        textOfReview: [
          '',
          [Validators.required, Validators.minLength(2), Validators.maxLength(100)]
        ],
        raitingOfReview: [
          '',
          [Validators.required, Validators.min(0), Validators.max(5)]
        ],
      });
  }

  public deleteReview(): void {
     this.reviewsService.deleteReview(this.review.bookId, this.review.id).subscribe(() => {
      this.notification.success('Review Deleted!');
      this.deleteEvent.emit(this.review.id)
      
    },
      () => this.notification.error('Invalid type of data!')
    );
  };

  public updateReview(bookId: number, reviewId: number, contentX: string, raitingX: number): void {
    this.reviewsService.updateReview(bookId, reviewId, contentX, raitingX)
      .subscribe(() => {
        this.notification.success('Review updated!');
        this.deleteEvent.emit(this.review.id)
        
      },
        () => this.notification.error('You have no permissions!')
      );
    }

}
