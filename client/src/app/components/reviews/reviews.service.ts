import { UpdateReviewDTO } from './models/update-review.dto';
import { AddReviewDTO } from './models/add-review.dto';
import { ShowReviewDTO } from './models/show-review.dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from 'src/app/common/config';

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {

  constructor(
    private readonly httpClient: HttpClient
    ) { }

  public getAllReviews(bookId: number): Observable<ShowReviewDTO[]> {

    return this.httpClient.get<ShowReviewDTO[]>(`${CONFIG.DOMAIN_NAME}/books/${bookId}/reviews`);
  }

  public getIndividualReview(reviewId: number): Observable<ShowReviewDTO> {

    return this.httpClient.get<ShowReviewDTO>(`${CONFIG.DOMAIN_NAME}/books/reviews/${reviewId}`);
  }


  public addReview(bookId: number, contentX: string, raitingX: number): Observable<any> {
    const review = {
      content: contentX,
      raiting: raitingX
    }
    return this.httpClient.post(`${CONFIG.DOMAIN_NAME}/books/${bookId}/reviews`, review);
  }

  public deleteReview(bookId: number, reviewId: number): Observable<any> {
    return this.httpClient.delete(`${CONFIG.DOMAIN_NAME}/books/${bookId}/reviews/delete/${reviewId}`);
  }

  public updateReview(bookId, reviewId: number, contentX: string, raitingX: number): Observable<any> {
    const review = {
      content: contentX,
      raiting: raitingX
    }
    return this.httpClient.put(`${CONFIG.DOMAIN_NAME}/books/${bookId}/reviews/${reviewId}`, review);
  }

}
