import { ReviewsService } from './../reviews.service';
import { ShowReviewDTO } from './../models/show-review.dto';
import { Component, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'app-all-reviews',
  templateUrl: './all-reviews.component.html',
  styleUrls: ['./all-reviews.component.css']
})
export class AllReviewsComponent implements OnInit {

  public reviews: ShowReviewDTO[];
  public idOfBook: number;
  constructor(
    private readonly reviewsService: ReviewsService,
    ) { }


  ngOnInit(): void {
    const urlAddres = location.href;
    const id = urlAddres.split('/').slice(-1).join('');
    this.idOfBook = +id;
    this.reviewsService.getAllReviews(+id).subscribe((foundReviews) => {
      this.reviews = foundReviews;
    });

  }

  public refresh(reviewId: number): void {
    this.reviewsService.getAllReviews(this.idOfBook).subscribe((foundReviews) => {
      this.reviews = foundReviews;
    });
  }

}
