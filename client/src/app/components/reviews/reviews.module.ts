import { ReviewsService } from './reviews.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleReviewComponent } from './single-review/single-review.component';
import { AllReviewsComponent } from './all-reviews/all-reviews.component';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material/material.module';
import { BookDetailpageComponent } from '../book-detailpage/book-detailpage.component';



@NgModule({
  declarations: [SingleReviewComponent, AllReviewsComponent, BookDetailpageComponent],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule

  ],
  providers: [ReviewsService],

  exports: [
    AllReviewsComponent,
    SingleReviewComponent,
    BookDetailpageComponent
  ],

})
export class ReviewsModule { }
