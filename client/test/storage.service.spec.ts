
import { TestBed } from "@angular/core/testing";
import { StorageService } from 'src/app/common/services/storage.service';

describe("StorageService", () => {
  let service: StorageService;

  beforeEach(() => {
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      providers: [StorageService],
    });

    service = TestBed.get(StorageService);
  });

  it("setItem() should call localStorage.setItem() once with correct parameters", () => {
    //Arrange
    const mockKey = "test";
    const mockValue = "data";
    jest.spyOn(localStorage, "setItem").mockImplementation(() => {});

    //Act
    service.setItem(mockKey, mockValue);

    //Assert
    expect(localStorage.setItem).toHaveBeenCalledTimes(1);
    expect(localStorage.setItem).toHaveBeenCalledWith("test", "data");
  });

  it("getItem() should call localStorage.getItem() once with correct parameter", () => {
    //Arrange
    const mockKey = "test";
    jest.spyOn(localStorage, "getItem").mockImplementation(() => "");

    //Act
    service.getItem(mockKey);

    //Assert
    expect(localStorage.getItem).toHaveBeenCalledTimes(1);
    expect(localStorage.getItem).toHaveBeenCalledWith("test");
  });

  it("getItem() should return null if there is no data stored", () => {
    //Arrange
    const mockKey = "test";
    jest.spyOn(localStorage, "getItem").mockImplementation(() => "undefined");

    //Act
    const result = service.getItem(mockKey);

    //Assert
    expect(result).toBe(null);
  });

  it("removeItem() should call localStorage.removeItem() once with correct parameter", () => {
    //Arrange
    const mockKey = "test";
    jest.spyOn(localStorage, "removeItem").mockImplementation(() => {});

    //Act
    service.removeItem(mockKey);

    //Assert
    expect(localStorage.removeItem).toHaveBeenCalledTimes(1);
    expect(localStorage.removeItem).toHaveBeenCalledWith(mockKey);
  });

  it("clear() should call localStorage.clear() once", () => {
    //Arrange
    jest.spyOn(localStorage, "clear").mockImplementation(() => {});

    //Act
    service.clear();

    //Assert
    expect(localStorage.clear).toHaveBeenCalledTimes(1);
    expect(localStorage.clear).toHaveBeenCalledWith();
  });
});
