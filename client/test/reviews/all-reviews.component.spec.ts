import { ShowReviewDTO } from './../../src/app/components/reviews/models/show-review.dto';
import { ReviewsService } from './../../src/app/components/reviews/reviews.service';
import { SingleReviewComponent } from './../../src/app/components/reviews/single-review/single-review.component';
import { AllReviewsComponent } from './../../src/app/components/reviews/all-reviews/all-reviews.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../../src/app/material/material.module';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { of, Observable } from 'rxjs';


describe('AllReviewsComponent', () => {

    let reviewsService;
    let fixture: ComponentFixture<AllReviewsComponent>;
    let component: AllReviewsComponent;

    beforeEach(async(() => {

        jest.clearAllMocks();

        reviewsService = {
            getAllReviews() { /* empty */ }
        };


        TestBed.configureTestingModule({

            declarations: [
                AllReviewsComponent,
                SingleReviewComponent,
            ],
            imports: [
                MaterialModule,
                RouterModule,
            ],
            providers: [ReviewsService]
        })
            .overrideProvider(ReviewsService, { useValue: ReviewsService })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(AllReviewsComponent);
                component = fixture.componentInstance;
            });
    }));

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(component).toBeDefined();
    });

    describe('ngOnInit() should', () => {

        it('call ReviewsService.getAllReviews once', () => {

            // Arrange
            const mockReview = new ShowReviewDTO();

            const mockTodos: ShowReviewDTO[] = [mockReview];

            const spy = jest.spyOn(reviewsService, 'getAllReviews').mockReturnValue(of(mockTodos));

            // Act
            component.ngOnInit();

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });
    });

});
