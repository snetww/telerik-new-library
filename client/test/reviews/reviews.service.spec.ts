import { ReviewsService } from './../../src/app/components/reviews/reviews.service';
import { of } from 'rxjs';
import { CONFIG } from '../../src/app/common/config';
import { async, TestBed } from '@angular/core/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';

describe('ReviewsService', () => {

    let httpClient;

    let service: ReviewsService;

    beforeEach(async(() => {

        jest.clearAllMocks();

        httpClient = {
            get() { /* empty */ },
            post() { /* empty */ },
            put() { /* empty */ },
            delete() { /* empty */ }
        };

        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [ReviewsService]
        }).overrideProvider(HttpClient, { useValue: httpClient });

        service = TestBed.get(ReviewsService);
    }));

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(service).toBeDefined();
    });

    describe('getAllReviews should', () => {

        it('call the httpClient.get() method once with correct parameters', done => {

            // Arrange
            const bookId = 1;
            const url = `${CONFIG.DOMAIN_NAME}/books/${bookId}/reviews`;
            const returnValue = of('retun value');

            const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            // Act & Assert
            service.getAllReviews(bookId).subscribe(() => {
                expect(spy).toHaveBeenCalledTimes(1);
                expect(spy).toHaveBeenCalledWith(url);

                done();
            });
        });

        it('return the result from the httpClient.get() method', () => {

            // Arrange
            const bookId = 1;
            const url = `${CONFIG.DOMAIN_NAME}/books/${bookId}/reviews`;
            const returnValue = of('retun value');

            const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            // Act
            const result = service.getAllReviews(bookId);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });

    describe('getIndividualReview should', () => {

        it('call the httpClient.get() method once with correct parameters', done => {

            // Arrange
            const reviewId = 1;
            const url = `${CONFIG.DOMAIN_NAME}/books/reviews/${reviewId}`;
            const returnValue = of('retun value');

            const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            // Act & Assert
            service.getIndividualReview(reviewId).subscribe(() => {
                expect(spy).toHaveBeenCalledTimes(1);
                expect(spy).toHaveBeenCalledWith(url);

                done();
            });
        });

        it('return the result from the httpClient.get() method', () => {

            // Arrange
            const reviewId = 1;
            const url = `${CONFIG.DOMAIN_NAME}/books/reviews/${reviewId}`;
            const returnValue = of('retun value');

            const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            // Act
            const result = service.getIndividualReview(reviewId);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });
    describe('addReview should', () => {

        it('call the httpClient.post() method once with correct parameters', done => {

            // Arrange
            const bookId = 1;
            const url = `${CONFIG.DOMAIN_NAME}/books/${bookId}/reviews`;
            const returnValue = of('retun value');

            const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);
            const mockedOne = {
                content: "dada",
                raiting: 10
            }
            const content = "dada"
            const raiting = 10;

            // Act & Assert
            service.addReview(bookId, content, raiting).subscribe(() => {
                expect(spy).toHaveBeenCalledTimes(1);
                expect(spy).toHaveBeenCalledWith(url, mockedOne);

                done();
            });
        });

        it('return the result from the httpClient.post() method', () => {

            // Arrange
            const bookId = 1;
            const url = `${CONFIG.DOMAIN_NAME}/books/${bookId}/reviews`;
            const returnValue = of('retun value');
            const content = "dada"
            const raiting = 10;

            const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);

            // Act
            const result = service.addReview(bookId, content, raiting);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });

    describe('updateReview should', () => {

        it('call the httpClient.put() method once with correct parameters', done => {

            // Arrange
            const bookId = 1;
            const reviewId = 1;
            const url = `${CONFIG.DOMAIN_NAME}/books/${bookId}/reviews/${reviewId}`;
            const returnValue = of('retun value');
            const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);
            const content = "dada"
            const raiting = 10;
            const mockedOne = {
                content: "dada",
                raiting: 10
            }

            // Act & Assert
            service.updateReview(bookId, reviewId, content, raiting).subscribe(() => {
                expect(spy).toHaveBeenCalledTimes(1);
                expect(spy).toHaveBeenCalledWith(url, mockedOne);

                done();
            });
        });

        it('return the result from the httpClient.put() method', () => {

            // Arrange
            const bookId = 1;
            const reviewId = 1;
            const url = `${CONFIG.DOMAIN_NAME}/books/${bookId}/reviews/${reviewId}`;
            const returnValue = of('retun value');
            const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);
            const content = "dada"
            const raiting = 10;

            // Act
            const result = service.updateReview(bookId, reviewId, content, raiting);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });

    describe('deleteReview should', () => {

        it('call the httpClient.delete() method once with correct parameters', done => {

            // Arrange
            const bookId = 1;
            const reviewId =1;
            const url = `${CONFIG.DOMAIN_NAME}/books/${bookId}/reviews/delete/${reviewId}`;
            const returnValue = of('retun value');

            const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(returnValue);

            // Act & Assert
            service.deleteReview(bookId, reviewId).subscribe(() => {
                expect(spy).toHaveBeenCalledTimes(1);
                expect(spy).toHaveBeenCalledWith(url);

                done();
            });
        });

        it('return the result from the httpClient.delete() method', () => {

            // Arrange
            const bookId = 1;
            const reviewId =1;
            const url = `${CONFIG.DOMAIN_NAME}/books/${bookId}/reviews/delete/${reviewId}`;
            const returnValue = of('retun value');

            const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(returnValue);

            // Act
            const result = service.deleteReview(bookId, reviewId);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });
});
