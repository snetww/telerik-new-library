import { AuthService } from "src/app/auth/auth.service";
import { TestBed, async } from "@angular/core/testing";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { JwtModule, JwtHelperService } from "@auth0/angular-jwt";
import { combineLatest, of } from "rxjs";
import { StorageService } from 'src/app/common/services/storage.service';
import { LoginUserDTO } from 'src/app/users/models/login-user.dto';
import { CONFIG } from 'src/app/common/config';


describe("AuthService", () => {
    let http;
    let storage;
    let jwtService;
  
    let service: AuthService;
  
    beforeEach(async(() => {
      jest.clearAllMocks();
  
      http = {
        get() {},
        post() {},
        delete() {},
      };
  
      storage = {
        read() {},
        save() {},
        clear() {},
      };
  
      jwtService = {
        decodeToken() {},
      };
  
      TestBed.configureTestingModule({
        imports: [HttpClientModule, JwtModule.forRoot({ config: {} })],
        providers: [AuthService, StorageService],
      })
        .overrideProvider(HttpClient, { useValue: http })
        .overrideProvider(StorageService, { useValue: storage })
        .overrideProvider(JwtHelperService, { useValue: jwtService });
  
      service = TestBed.get(AuthService);
    }));
  
    it("should be defined", () => {
      expect(service).toBeDefined();
    });
  
    it("handle correctly invalid (or empty/none) tokens", (done) => {
      // Arrange
      jest.spyOn(jwtService, "decodeToken").mockImplementation(() => undefined);
  
      // Act & Assert
      combineLatest(service.isLoggedIn$, service.loggedUser$).subscribe(
        ([loggedIn, loggedUser]) => {
          expect(loggedIn).toBe(false);
          expect(loggedUser).toEqual(undefined);
  
          done();
        }
      );
    });
  

    describe("login should", () => {
      it("call http.post() once with correct parameters", () => {
        // Arrange
        const token = "";
        const user = new LoginUserDTO();
        const url = `${CONFIG.DOMAIN_NAME}/login`;
  
        const spy = jest.spyOn(http, "post").mockReturnValue(of(token));
  
        // Act
        service.login(user);
  
        // Assert
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url, user);
      });
  
      it("http.post() return correct value", (done) => {
        // Arrange
        const token = { token: "" };
        const user = new LoginUserDTO();
  
        jest.spyOn(http, "post").mockReturnValue(of(token));
  
        // Act & Assert
        service.login(user).subscribe((result) => {
          expect(result).toEqual(token);
  
          done();
        });
      });
  
      it("call jwtService.decodeToken() once with correct parameters", (done) => {
        // Arrange
        const mockedToken = { token: "token" };
        const user = new LoginUserDTO();
  
        jest.spyOn(http, "post").mockReturnValue(of(mockedToken));
        const spy = jest.spyOn(jwtService, "decodeToken");
  
        // Act & Assert
        service.login(user).subscribe(({ token }) => {
          expect(spy).toHaveBeenCalledTimes(1);
          expect(spy).toHaveBeenCalledWith(token);
  
          done();
        });
      });
  
      it("call storage.save() once with correct parameters", (done) => {
        // Arrange
        const mockedToken = { token: "token" };
        const user = new LoginUserDTO();
  
        jest.spyOn(http, "post").mockReturnValue(of(mockedToken));
        const spy = jest.spyOn(storage, "save");
  
        // Act & Assert
        service.login(user).subscribe(({ token }) => {
          expect(spy).toHaveBeenCalledTimes(1);
          expect(spy).toHaveBeenCalledWith("token", token);
  
          done();
        });
      });
      });
    });
